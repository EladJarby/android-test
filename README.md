# Android Programming Task #

In order to be considered for the Android position, you must complete the following steps.
Note: Try to finish this test in two hours.
Note: If you are not familiar with Android at all then you may write this exercise as a desktop app with any programming language of your choosing.

### Prerequisites ###

* Knowledge of Android and Android Studio.
* Knowledge of Java.
* You will need to have Java, Android Studio, and the Android SDK installed.

# Task #

1. Fork this repository.
2. Create an Android app that accomplishes the following:
3. Get a list of youtube videos grouped into playlists http://www.leado.co.il/clients/shahak/json.json
4. Create a list view that shows each of the playlists as list items.
5. When a playlist item is tapped, the app should drill down and show each item of the selected playlist with it's corresponding thumbnail image.
6. When a video item is tapped, the youtube video should begin playing immediately. We would prefer that you do not use a web view for this (use a youtube library instead).Advice: Start with web view (or intent) and then move to a customize component.
7. The user should be able to navigate back to the list view after viewing the youtube video - and continue from where he left off!!
8. Commit and Push your code to your new repository
9. Send us a pull request for your repository, we will review your code and get back to you


# Judging #

* Code quality: testability, encapsulation, design patterns (MVP), comments, readable code, clean code, etc.
* UI/UX: This is the place for you to show your creativity.

# Advice and Bonus #

* You are encourage to use libraries to help you produce better code.
 * volley/retrofit for rest api.
 * ui list views
 * Just check Android Arsenal
* You should use RecyclerView for the list.
* MVP is the recommended pattern (but the future is MVVM)
* Bonus: inject your dependencies (not required to use an injection library) and add a mock flavor with some lists of your own.